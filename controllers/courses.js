//[SECTION] Dependencies and Modules
  	const Course = require('../models/Course');

//[SECTION] Functionality [Create] 
	module.exports.addCourse = (info) => {
		let cName = info.name;
		let cDesc = info.description;
		let cCost = info.price;
		let newCourse = new Course({
			name: cName,
			description:cDesc,
			price: cCost
		}); 
		return newCourse.save().then((savedCourse, err) => {
			if (savedCourse) {
				return savedCourse; 
			} else {
				return false;
			}
		});
	}

//[SECTION] Functionality [Retrieve] 
   module.exports.getAllCourse = () => {
   		return Course.find({}).then(result => {
   			return result; 
   		});
   };

   module.exports.getAllActive = () => {
   		return Course.find({isActive: true}).then(result => {
   			return result;
   		});
   };

   module.exports.getCourse = (id) => { 
   		return Course.findById(id).then(result => {
   			return result; 
   		});
   };

//[SECTION] Functionality [Update]
    module.exports.updateCourse = (course, details) => {
		let cName = details.name;
		let cDesc = details.description;
		let cCost = details.price;
		let updatedCourse = {		
			name: cName,
			description: cDesc,
			price: cCost
		}; 
		let id = course.courseId;
		return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
			if (courseUpdated) {
				return true; 
			} else {
				return 'Failed to Update Course';
			};
		}); 
	};

	module.exports.archiveCourse = (course) => {
	    let id = course.courseId; 
	    let updates = {
	   	  isActive: false 
	    } 
		return Course.findByIdAndUpdate(id, updates).then((archived, err) => {
			if (archived) {
				return 'Course archived'; 
			} else {
				return false; 
			}
		});
	};

//[SECTION] Functionality [Delete]

	module.exports.deleteCourse = (courseId) => {
		return Course.findByIdAndRemove(courseId).then((removedCourse, err) => {
			//control structure to identify the responses if both tasks were performed successfully on a resourse. 
			if (removedCourse) {
				return 'Course successfully removed';
			} else {
				return 'No Course was Removed';
			};
		});
	};

	//findByIdAndRemove()
