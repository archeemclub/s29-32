//[SECTION] Dependencies and Modules
	const exp = require('express'); 
	const controller = require('../controllers/users');
	const auth = require('../auth'); 

//[SECTION] Routing Component
	const route = exp.Router();

//[SECTION] Routes-[POST]
	route.post('/register', (req, res) => {
		let userData = req.body;
		controller.registerUser(userData).then(outcome => {
			res.send(outcome);
		}); 
	});

	route.post('/check-email', (req, res) => {
		controller.checkEmailExists(req.body).then(outcome => {
			res.send(outcome);
		});
	});
	
	route.post('/login', (req, res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome);
		});
	});

//[SECTION] Routes-[GET]
	route.get('/details', auth.verify ,(req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		});
	});


//[SECTION] Routes-[UPDATE]
   route.put('/:userId/set-as-admin', auth.verify, (req, res) => {
   	    let token = req.headers.authorization;
   	    let payload = auth.decode(token); 
   	    let isAdmin = payload.isAdmin;
   		let id = req.params.userId; 
   		(isAdmin) ? controller.setAsAdmin(id).then(outcome => res.send(outcome))
   		: res.send('Unauthorized User');
   });
   
   route.put('/:userId/set-as-user', auth.verify ,(req, res) => {
   	   let token = req.headers.authorization;
   	   let isAdmin = auth.decode(token).isAdmin;
   	   let id = req.params.userId;
   	   isAdmin ? controller.setAsNonAdmin(id).then(result => res.send(result)) 
   	   : res.send('Unauthorized User');  	   
   });
   


//[SECTION] Routes-[DELETE]

//[SECTION] Expose Route System
	module.exports = route; 

